<?php

namespace Tests\LoadBalancer;

use App\Host\HostInterface;
use App\Host\HostsList;
use App\LoadBalancer\Algorithms\AlgorithmInterface;
use App\LoadBalancer\LoadBalancer;
use App\Request\Request;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class LoadBalancerTest extends TestCase
{
    /**
     * @var HostsList
     */
    private $hostList;

    /**
     * @var HostInterface|MockObject
     */
    private $host;

    /**
     * @var Request
     */
    private $request;

    /**
     * @var AlgorithmInterface
     */
    private $algorithm;

    /**
     * @var LoadBalancer
     */
    private $loadBalancer;

    public function testHandleRequest()
    {
        $this->host
            ->expects($this->once())
            ->method('handleRequest')
            ->with($this->request);

        $this->loadBalancer->handleRequest($this->request);
    }

    protected function setUp()
    {
        $this->host = $this->getMockBuilder(HostInterface::class)->getMock();

        $this->hostList = $this->getMockBuilder(HostsList::class)->disableOriginalConstructor()->getMock();

        $this->algorithm = $this->getMockBuilder(AlgorithmInterface::class)->getMock();
        $this->algorithm->expects($this->any())->method('getHost')->willReturn($this->host);

        $this->request = $this->
        getMockBuilder(Request::class)->disableOriginalConstructor()->getMock();

        $this->loadBalancer = new LoadBalancer($this->hostList, $this->algorithm);
    }
}
