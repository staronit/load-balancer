<?php

namespace Tests\LoadBalancer\Algorithms;

use App\Host\HostInterface;
use App\Host\HostsList;
use App\LoadBalancer\Algorithms\AlgorithmInterface;
use App\LoadBalancer\Algorithms\LoadAlgorithm;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class LoadAlgorithmTest extends TestCase
{
    /**
     * @var HostInterface
     */
    private $host1;

    /**
     * @var HostInterface
     */
    private $host2;

    /**
     * @var HostInterface
     */
    private $host3;

    /**
     * @var HostInterface
     */
    private $host4;

    /**
     * @var HostsList|MockObject
     */
    private $hostListWithOneBelowLimit;

    /**
     * @var HostsList|MockObject
     */
    private $hostListWithAllAboveLimit;

    /**
     * @var AlgorithmInterface
     */
    private $algorithm;

    public function testGetHostBelowLimit()
    {
        $host = $this->algorithm->getHost($this->hostListWithOneBelowLimit);
        $this->assertSame($this->host4, $host);
    }

    public function testGetHostAboveLimit()
    {
        $host = $this->algorithm->getHost($this->hostListWithAllAboveLimit);
        $this->assertSame($this->host2, $host);
    }

    protected function setUp()
    {
        $this->host1 = $this->getMockBuilder(HostInterface::class)->getMock();
        $this->host1->expects($this->any())->method('getLoad')->willReturn(0.99);

        $this->host2 = $this->getMockBuilder(HostInterface::class)->getMock();
        $this->host2->expects($this->any())->method('getLoad')->willReturn(0.75);

        $this->host3 = $this->getMockBuilder(HostInterface::class)->getMock();
        $this->host3->expects($this->any())->method('getLoad')->willReturn(0.94);

        $this->host4 = $this->getMockBuilder(HostInterface::class)->getMock();
        $this->host4->expects($this->any())->method('getLoad')->willReturn(0.65);

        $this->hostListWithOneBelowLimit = $this->getMockBuilder(HostsList::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->hostListWithOneBelowLimit->expects($this->any())->method('getFirst')->willReturn($this->host1);
        $this->hostListWithOneBelowLimit->expects($this->any())->method('getHosts')->willReturn([
            $this->host1,
            $this->host2,
            $this->host3,
            $this->host4,
        ]);

        $this->hostListWithAllAboveLimit = $this->getMockBuilder(HostsList::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->hostListWithAllAboveLimit->expects($this->any())->method('getFirst')->willReturn($this->host1);
        $this->hostListWithAllAboveLimit->expects($this->any())->method('getHosts')->willReturn([
            $this->host1,
            $this->host2,
            $this->host3,
        ]);

        $this->algorithm = new LoadAlgorithm();
    }
}
