<?php

namespace Tests\LoadBalancer\Algorithms;

use App\Host\HostInterface;
use App\Host\HostsList;
use App\LoadBalancer\Algorithms\AlgorithmInterface;
use App\LoadBalancer\Algorithms\RoundRobinAlgorithm;
use PHPUnit\Framework\TestCase;

class RoundRobinAlgorithmTest extends TestCase
{
    /**
     * @var HostInterface
     */
    private $host1;

    /**
     * @var HostInterface
     */
    private $host2;

    /**
     * @var HostInterface
     */
    private $host3;

    /**
     * @var HostsList
     */
    private $hostsList;

    /**
     * @var AlgorithmInterface
     */
    private $algorithm;

    public function testGetHost()
    {
        $host = $this->algorithm->getHost($this->hostsList);
        $this->assertSame($this->host1, $host);

        $host = $this->algorithm->getHost($this->hostsList);
        $this->assertSame($this->host2, $host);

        $host = $this->algorithm->getHost($this->hostsList);
        $this->assertSame($this->host3, $host);

        $host = $this->algorithm->getHost($this->hostsList);
        $this->assertSame($this->host1, $host);
    }

    protected function setUp()
    {
        $this->host1 = $this->getMockBuilder(HostInterface::class)->getMock();
        $this->host1->expects($this->any())->method('getLoad')->willReturn(1.0);

        $this->host2 = $this->getMockBuilder(HostInterface::class)->getMock();
        $this->host1->expects($this->any())->method('getLoad')->willReturn(2.0);

        $this->host3 = $this->getMockBuilder(HostInterface::class)->getMock();
        $this->host1->expects($this->any())->method('getLoad')->willReturn(3.0);

        $this->hostsList = $this->getMockBuilder(HostsList::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->hostsList->expects($this->at(0))->method('getNext')->willReturn($this->host1);
        $this->hostsList->expects($this->at(1))->method('getNext')->willReturn($this->host2);
        $this->hostsList->expects($this->at(2))->method('getNext')->willReturn($this->host3);
        $this->hostsList->expects($this->at(3))->method('getNext')->willReturn($this->host1);


        $this->algorithm = new RoundRobinAlgorithm();
    }
}
