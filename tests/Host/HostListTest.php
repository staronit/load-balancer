<?php

namespace Tests\Host;

use App\Host\HostInterface;
use App\Host\HostsList;
use PHPUnit\Framework\TestCase;

class HostListTest extends TestCase
{
    /**
     * @var HostInterface
     */
    private $host1;

    /**
     * @var HostInterface
     */
    private $host2;

    /**
     * @var HostInterface
     */
    private $host3;

    /**
     * @var HostsList
     */
    private $hostList;


    public function testGetFirst()
    {
        $first = $this->hostList->getFirst();
        $this->assertSame($this->host1, $first);
    }

    public function testGetHosts()
    {
        $hosts = $this->hostList->getHosts();
        $this->assertInternalType('array', $hosts);
        $this->assertContainsOnlyInstancesOf(HostInterface::class, $hosts);
        $this->assertCount(3, $hosts);
    }

    public function testGetNext()
    {
        $next = $this->hostList->getNext();
        $this->assertSame($this->host1, $next);

        $next = $this->hostList->getNext();
        $this->assertSame($this->host2, $next);

        $next = $this->hostList->getNext();
        $this->assertSame($this->host3, $next);

        $next = $this->hostList->getNext();
        $this->assertSame($this->host1, $next);
    }

    /**
     * @expectedException \App\Host\Exception\HostException
     */
    public function testCreateWithEmptyArray()
    {
        new HostsList([]);
    }

    protected function setUp()
    {
        $this->host1 = $this->getMockBuilder(HostInterface::class)->getMock();
        $this->host2 = $this->getMockBuilder(HostInterface::class)->getMock();
        $this->host3 = $this->getMockBuilder(HostInterface::class)->getMock();

        $this->hostList = new HostsList([
            $this->host1,
            $this->host2,
            $this->host3
        ]);
    }
}
