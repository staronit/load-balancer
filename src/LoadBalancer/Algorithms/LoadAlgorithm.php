<?php

namespace App\LoadBalancer\Algorithms;

use App\Host\HostInterface;
use App\Host\HostsList;

class LoadAlgorithm implements AlgorithmInterface
{
    const LOAD_LIMIT = 0.75;

    /**
     * @inheritDoc
     */
    public function getHost(HostsList $hosts): HostInterface
    {
        $currentHost = $hosts->getFirst();
        foreach ($hosts->getHosts() as $host) {
            if ($host->getLoad() < self::LOAD_LIMIT) {
                return $host;
            }

            if ($host->getLoad() < $currentHost->getLoad()) {
                $currentHost = $host;
            }
        }

        return $currentHost;
    }
}
