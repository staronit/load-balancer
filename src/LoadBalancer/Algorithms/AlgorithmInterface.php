<?php

namespace App\LoadBalancer\Algorithms;

use App\Host\HostInterface;
use App\Host\HostsList;

interface AlgorithmInterface
{
    /**
     * @param HostsList $hosts
     * @return HostInterface
     */
    public function getHost(HostsList $hosts): HostInterface;
}
