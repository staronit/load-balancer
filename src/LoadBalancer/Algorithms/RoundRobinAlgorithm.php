<?php


namespace App\LoadBalancer\Algorithms;

use App\Host\HostInterface;
use App\Host\HostsList;

class RoundRobinAlgorithm implements AlgorithmInterface
{
    /**
     * @inheritDoc
     */
    public function getHost(HostsList $hosts): HostInterface
    {
        return $hosts->getNext();
    }
}
