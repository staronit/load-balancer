<?php

namespace App\LoadBalancer;

use App\Host\HostsList;
use App\LoadBalancer\Algorithms\AlgorithmInterface;
use App\Request\Request;

class LoadBalancer
{
    /**
     * @var HostsList
     */
    private $hosts;

    /**
     * @var AlgorithmInterface
     */
    private $algorithm;

    /**
     * LoadBalancer constructor.
     * @param HostsList $hosts
     * @param AlgorithmInterface $algorithm
     */
    public function __construct(HostsList $hosts, AlgorithmInterface $algorithm)
    {
        $this->hosts = $hosts;
        $this->algorithm = $algorithm;
    }

    /**
     * @inheritDoc
     */
    public function handleRequest(Request $request): void
    {
        $host = $this->algorithm->getHost($this->hosts);
        $host->handleRequest($request);
    }
}
