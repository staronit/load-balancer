<?php


namespace App\Host;

use App\Host\Exception\HostException;

class HostsList
{
    const INDEX_FIRST = 0;

    /**
     * @var HostInterface[]
     */
    private $hosts;

    /**
     * HostsList constructor.
     * @param HostInterface[] $hosts
     * @throws HostException
     */
    public function __construct(array $hosts)
    {
        if (empty($hosts)) {
            throw new HostException('Hosts cannot be empty');
        }

        $this->hosts = array_values($hosts);
    }

    /**
     * @return HostInterface[]
     */
    public function getHosts(): array
    {
        return $this->hosts;
    }

    /**
     * @return HostInterface
     */
    public function getFirst(): HostInterface
    {
        return $this->hosts[self::INDEX_FIRST];
    }

    /**
     * @return HostInterface
     */
    public function getNext(): HostInterface
    {
        $host = current($this->hosts);
        if (next($this->hosts) === false) {
            reset($this->hosts);
        }

        return $host;
    }
}
