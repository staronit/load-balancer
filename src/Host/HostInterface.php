<?php


namespace App\Host;

use App\Request\Request;

interface HostInterface
{
    /**
     * @return float
     */
    public function getLoad(): float;

    /**
     * @param Request $request
     */
    public function handleRequest(Request $request): void;
}
